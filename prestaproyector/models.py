import os
from datetime import datetime, timedelta

#from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin 
#from django.contrib.auth.base_user import BaseUserManager
from django.db import models
from django import forms


class Proyector(models.Model):
    id_proyector = models.IntegerField(primary_key=True)
    descripcion = models.CharField(max_length=50, blank=True)

    disponible = models.BooleanField(default=True)
    
    def __str__(self):
        return 'Proyector ' + str(self.id_proyector)

class Materia(models.Model):
    id_materia = models.CharField(max_length=15, primary_key=True)
    nombre = models.CharField(max_length=30, blank=True, null=True)

    def __str__(self):
        return self.nombre

class Componente(models.Model):
    id_componente = models.IntegerField(primary_key=True)
    nombre_componente = models.CharField(max_length=15, blank=True, null=True)
    tipo_componente = models.CharField(max_length=15, blank=True, null=True)
    descripcion = models.CharField(max_length=150, blank=True, null=True)
    proyector = models.ForeignKey(Proyector, default=None, on_delete=models.CASCADE, null=True, blank=True)
    disponible = models.BooleanField(default=True)

    def __str__(self):
        return "Componente "+str(self.id_componente)+": "+str(self.nombre_componente)

class Solicitante(models.Model):
    matricula = models.CharField(max_length=10, blank=False, primary_key=True, unique=True)
    nombre = models.CharField(max_length=20, blank=False)
    ap_pat = models.CharField(max_length=20, blank=False, null=True)
    ap_mat = models.CharField(max_length=20, blank=False, null=True)
    carrera = models.CharField(max_length=30, blank=False, null=True )
    #materia = models.CharField(max_length=30, blank=True, null=True)
    materias = models.ManyToManyField(Materia, through='Solicitante_Materia')
    bloqueado = models.BooleanField(default=False)

    def __str__(self):
        return self.matricula


class Solicitante_Materia(models.Model):
    solicitante = models.ForeignKey(Solicitante, default=None, on_delete=models.CASCADE, db_column='matricula')
    materia = models.ForeignKey(Materia, default=None, on_delete=models.CASCADE, db_column='id_materia')

    class Meta:
        db_table = 'prestaproyector_sol_materia'




class Prestamo(models.Model):
    proyector = models.ForeignKey(Proyector, default=None, blank=True, null=True, on_delete=models.CASCADE)
    solicitante = models.ForeignKey(Solicitante, default=None, on_delete=models.CASCADE)
    #materia = models.ForeignKey(Materia, default=None, on_delete=models.CASCADE)
    componentes = models.ManyToManyField(Componente, default=None, blank=True)
    materia = models.CharField(max_length=30, blank=False, null=True)
    aula = models.CharField(max_length=20, blank=False)
    fecha_hora = models.DateTimeField(auto_now_add=True)
    hora_fin = models.DateTimeField()
    incidencia = models.BooleanField(default=False)
    cerrado = models.BooleanField(default=False)



    def __str__(self):
        return "Prestamo " + str(self.id)


