import datetime
import csv
from datetime import date, timedelta
from django.db.models import Count
from django.utils import timezone
from django.utils.safestring import mark_safe
from django.http import HttpResponse
from django.shortcuts import render
from django.shortcuts import redirect
from django.contrib import messages
from .models import Prestamo, Proyector, Solicitante, Componente
from .forms import SolicitudProyector_Form, RegistroMateria_Form, RegistroSolicitante_Form, RegistroProyector_Form, RegistroComponente_Form
from django.contrib.auth import login, logout
from django.core.paginator import Paginator
from django.contrib.auth.decorators import login_required, permission_required

@login_required(login_url="/prestaproyector/accounts/login/")
def dashboard(request):
    today = timezone.now()

    vigentes = Prestamo.objects.all().filter(hora_fin__gt = datetime.datetime.now(), cerrado=False)

    expirados = Prestamo.objects.filter(hora_fin__lt = datetime.datetime.now(), cerrado = False)
    prestamos = Prestamo.objects.all().order_by('-fecha_hora')
    cerrados = Prestamo.objects.all().filter(cerrado = True)
    incidencias = Prestamo.objects.all().filter(incidencia = True)
    #bloqueados = Solicitante.objects.all().filter()

    return render(request, 'dashboard.html', {'vigentes': vigentes, 'expirados':expirados ,'prestamos':prestamos, 'cerrados':cerrados, 'today':today, 'incidencias': incidencias})

def lista_prestamos(request):
    today = timezone.now()

    vigentes = Prestamo.objects.all().filter(hora_fin__gt = datetime.datetime.now(), cerrado=False)

    expirados = Prestamo.objects.filter(hora_fin__lt = datetime.datetime.now(), cerrado = False)
    prestamos = Prestamo.objects.all().order_by('-fecha_hora')
    cerrados = Prestamo.objects.all().filter(cerrado = True)
    incidencias = Prestamo.objects.all().filter(incidencia = True)
    #bloqueados = Solicitante.objects.all().filter()

    return render(request, 'lista_solicitantes.html', {'vigentes': vigentes, 'expirados':expirados ,'prestamos':prestamos, 'cerrados':cerrados, 'today':today, 'incidencias': incidencias})

@login_required(login_url="/prestaproyector/accounts/login/")
def status_proyectores(request):
    todos = Proyector.objects.all()
    todos_c = Componente.objects.all()
    no_disponibles = Proyector.objects.all().filter( disponible = False )
    no_disponibles_c = Componente.objects.all().filter( disponible = False )
    disponibles = Proyector.objects.all().filter(disponible = True)
    disponibles_c = Componente.objects.all().filter( disponible = False )
    return render(request, 'proyectores.html', {'todos':todos, 'todos_c':todos_c, 'no_disponibles':no_disponibles, 'no_disponibles_c':no_disponibles_c, 'disponibles':disponibles, 'disponibles_c':disponibles_c})

@login_required(login_url="/prestaproyector/accounts/login/")
def registrar_proyector(request):
    if request.method == 'POST':
        form = RegistroProyector_Form(request.POST)

        if form.is_valid():
            messages.success(request, 'Proyector Registrado Correctamente')
            instance = form.save(commit=False)
            instance.save()
            return redirect('proyectores')
        else:
            messages.error(request, 'Error al Registrar Proyector')
            
    else:
        form = RegistroProyector_Form()

    return render(request, 'add_proyector.html', {'form': form})

@login_required(login_url="/prestaproyector/accounts/login/")
def registrar_componente(request):
    if request.method == 'POST':
        form = RegistroComponente_Form(request.POST)

        if form.is_valid():
            messages.success(request, 'Componente Registrado Correctamente')
            instance = form.save(commit=False)
            instance.save()
            return redirect('proyectores') #Cambiar A componentes
        else:
            messages.error(request, 'Error al registrar Componente')
    else:
        form = RegistroComponente_Form()
    
    return render(request, 'add_componente.html', {'form':form})


def registrar_solicitante(request):
    if request.method == 'POST':
        form = RegistroSolicitante_Form(request.POST)

        if form.is_valid():
            messages.success(request, 'Solicitante Registrado Correctamente')
            instance = form.save(commit=False)
            instance.save()
            return redirect('solicitantes')
        else:
            messages.error(request, 'Error al Registrar')
            
    else:
        form=RegistroSolicitante_Form()

    return render(request, 'add_solicitante.html', {'form': form})

    

def solicitud(request):
    today = timezone.now()
    componentes = Componente.objects.all()
    #disponibles = Proyector.objects.all().filter(disponible = True)
    #no_disponibles = Proyector.objects.all().filter(disponible = False)
    abiertos = Prestamo.objects.all().filter(cerrado = False)
    incidentes = Prestamo.objects.values('solicitante').annotate(solicitante_count=Count('solicitante')).filter(solicitante_count__gt=0, incidencia=True).values('solicitante')
    incidentes_count = Prestamo.objects.values('solicitante').annotate(solicitante_count=Count('solicitante')).filter(solicitante_count__gt=0, incidencia=True).values('solicitante_count')
    sol_incidentes = Solicitante.objects.filter(matricula__in=incidentes)

    #checar si el solicitante ya tiene su tercera incidencia





    if request.method =='POST':
        solicitante_matricula = request.POST.get('solicitante')
    #else:
    #    solicitante_matricula = request.GET.get('solicitante')

    #checar si el solicitante ya tiene su tercera incidencia   
  
#    if request.method == 'POST':
#        try:
#            solicitante = Solicitante.objects.get(matricula=solicitante_matricula)
#        except Solicitante.DoesNotExist:
#            messages.error(request, 'No. de Control'+ str(solicitante_matricula) + ' no existente en el sistema. Consulte a asistente para registrar al solicitante.')
#            return redirect('solicitud')
#        if solicitante.bloqueado == True:
#            messages.error(request, 'Solicitud Denegada. Solicitante Bloqueado.')
#            return redirect('solicitud')
#        else:
#                return redirect('sol_proyector')


    return render(request, 'home.html', { 'abiertos':abiertos, 'componentes':componentes,  'today': today})

def prefill_solicitud(request):

    #filtrar componentes de proyector

        
    componentes = Componente.objects.filter(disponible = True)
    a_bloquear = Prestamo.objects.values('solicitante').annotate(solicitante_count=Count('solicitante')).filter(solicitante_count__gt=2, incidencia=True).values('solicitante')
    sol_a_bloquear = Solicitante.objects.filter(matricula__in=a_bloquear)



    if request.method == 'POST':
        try:
            solicitante = Solicitante.objects.get(matricula=request.POST.get('solicitante'))
            if solicitante.bloqueado == True:
                messages.error(request, 'Solicitud Denegada. Solicitante Bloqueado.')
                return redirect('solicitud')
        except Solicitante.DoesNotExist:
            messages.error(request, mark_safe('No. de Control '+str(request.POST.get('solicitante')) + ' no existente en el sistema. <strong>Verifique que lo haya ingresado correctamente</strong> o <a href="/prestaproyector/registrar_solicitante/">Registrese aquí</a>'))
            return redirect('solicitud')

        


    disponibles = Proyector.objects.all().filter(disponible = True)
    no_disponibles = Proyector.objects.all().filter(disponible = False)
    abiertos = Prestamo.objects.all().filter(cerrado = False)
    incidentes = Prestamo.objects.values('solicitante').annotate(solicitante_count=Count('solicitante')).filter(solicitante_count__gt=0, incidencia=True).values('solicitante')
    incidentes_count = Prestamo.objects.values('solicitante').annotate(solicitante_count=Count('solicitante')).filter(solicitante_count__gt=0, incidencia=True).values('solicitante_count')
    sol_incidentes = Solicitante.objects.filter(matricula__in=incidentes)



    if request.method =='POST':
        solicitante_matricula = request.POST.get('solicitante')

    solicitante = Solicitante.objects.get(matricula=solicitante_matricula)
    
    try:
        if sol_a_bloquear.filter(matricula=solicitante.matricula).exists() and solicitante.bloqueado == False:
                solicitante.bloqueado = True
                solicitante.save()
                messages.error(request,'Solicitud Denegada. Solicitante ha sido Bloqueado.')
                return redirect('solicitud')
        elif abiertos.filter(solicitante=solicitante.matricula).exists():
            messages.error(request,'Solicitud Denegada. Solicitante no ha hecho devolución.')
            return redirect('solicitud')
        
        sol_incidentes.get(matricula=solicitante.matricula)
        incidentes_count = Prestamo.objects.values('solicitante').annotate(solicitante_count=Count('solicitante')).filter(solicitante_count__gt=0,solicitante=solicitante.matricula, incidencia=True).values('solicitante_count')
        messages.warning(request, 'Solicitante tiene ' + str(incidentes_count.count()) + " incidencia(s)")
    except Solicitante.DoesNotExist:
        pass




    materias = solicitante.materias.all()
    form = SolicitudProyector_Form()

    return render(request, 'sol_proyector.html', { 'form': form, 'disponibles':disponibles, 'no_disponibles': no_disponibles, 'abiertos':abiertos, 'materias':materias, 'solicitante':solicitante, 'componentes': componentes })

def solicitud_proyector(request):
    disponibles = Proyector.objects.all().filter(disponible = True)
    no_disponibles = Proyector.objects.all().filter(disponible = False)
    abiertos = Prestamo.objects.all().filter(cerrado = False)
    incidentes = Prestamo.objects.values('solicitante').annotate(solicitante_count=Count('solicitante')).filter(solicitante_count__gt=0, incidencia=True).values('solicitante')
    incidentes_count = Prestamo.objects.values('solicitante').annotate(solicitante_count=Count('solicitante')).filter(solicitante_count__gt=0, incidencia=True).values('solicitante_count')
    sol_incidentes = Solicitante.objects.filter(matricula__in=incidentes)

    if request.method == 'POST':
        form = SolicitudProyector_Form(request.POST)
        if form.is_valid():
            solicitud = form.save()
            #solicitud.solicitante = solicitante
            solicitud.fecha_hora=timezone.now()
            if solicitud.proyector is not None:
                proyector_sel=Proyector.objects.filter(id_proyector=solicitud.proyector.id_proyector)

                #proyector_sel.disponible = False
                proyector_sel.update(disponible=False)
            if solicitud.componentes is not None:
                #componentes del proyector
                for componente in solicitud.componentes.all():
                    componentes_sel=Componente.objects.filter(id_componente=componente.id_componente)
                    componentes_sel.update(disponible = False)

            solicitud.save()
            messages.success(request, 'Aprobada')
            return redirect('solicitud')
    else:
        form = SolicitudProyector_Form()
    
    return render(request, 'sol_proyector.html', { 'form': form, 'disponibles':disponibles, 'no_disponibles': no_disponibles, 'abiertos':abiertos, 'materias':materias, 'solicitante':solicitante })


def devolucion(request, id): #convertir a post request
    prestamo_abierto = Prestamo.objects.get(id=id, cerrado=False)
    if prestamo_abierto.proyector is not None:
        proyector_sel = Proyector.objects.filter(id_proyector = prestamo_abierto.proyector.id_proyector)
        proyector_sel.update(disponible=True)

    if prestamo_abierto.componentes is not None:
        prestamo_abierto.componentes.update(disponible=True)
        
    prestamo_abierto.cerrado = True
    prestamo_abierto.save()
    messages.success(request, 'Devolucion Exitosa.')
    return redirect('solicitud')

@login_required(login_url="/prestaproyector/accounts/login/")
def marcar_incidencia(request, id):
    if request.method == 'POST':
        try:
            prestamo_expirado = Prestamo.objects.get(id=id, cerrado=False)
            if prestamo_expirado.incidencia == False:
                prestamo_expirado.incidencia = True
                messages.warning(request, 'Incidencia Marcada.')
            else:
                prestamo_expirado.incidencia = False
                messages.warning(request, 'Incidencia Removida.')

            prestamo_expirado.save()
            
            return redirect('lista_solicitantes')
        except Prestamo.DoesNotExist:
            return HttpResponse('Prestamo no encontrado', status=404)
        except Exception:
            return HttpResponse('Internal Error', status=500)
    return HttpResponse('Solo POST requests', status=405)

@login_required(login_url="/prestaproyector/accounts/login/")
def proyector_status_update(request, id):
    if request.method == 'POST':
        try:
            proyector_sel = Proyector.objects.get(id_proyector = id)
            if proyector_sel.disponible == False:
                proyector_sel.disponible = True
            else:
                proyector_sel.disponible = False
            
            proyector_sel.save()
            return redirect('proyectores')
        except Proyector.DoesNotExist:
            return HttpResponse('Proyector no encontrado', status=404)
        except Exception:
            return HttpResponse('Internal Error', status=500)
    return HttpResponse('Solo POST requests', status=405)

@login_required(login_url="/prestaproyector/accounts/login/")
def componente_status_update(request,id):
    if request.method == 'POST':
        try:
            componente_sel = Componente.objects.get(id_componente = id)
            if componente_sel.disponible == False:
                componente_sel.disponible = True
            else:
                componente_sel.disponible = False
            
            componente_sel.save()
            return redirect('proyectores')
        except Componente.DoesNotExist:
            return HttpResponse('Componente no encontrado', status=404)
        except Exception:
            return HttpResponse('Internal Error', status=500)
    return HttpResponse('Solo POST requests!', status=405)

@login_required(login_url="/prestaproyector/accounts/login/")
def solicitantes(request):
    #solicitantes = Solicitante.objects.all()
    #prestamos_incidencias = Prestamo.objects.filter(incidencia=True)
    #bloqueados = Solicitante.objects.filter(bloqueado = True)
    todos = Solicitante.objects.all()
    #cant_incidencias = Prestamo.objects.values('solicitante').annotate(solicitante_count=Count('solicitante')).filter(incidencia=True).values_list('solicitante_count', flat=True)
    incidentes_all = Prestamo.objects.values('solicitante').annotate(solicitante_count=Count('solicitante')).filter(solicitante_count__gt=0, incidencia=True).values_list('solicitante_count', flat=True)


    
    regulares = Prestamo.objects.values('solicitante').annotate(solicitante_count=Count('solicitante')).filter(solicitante_count__gt=0, incidencia=False).values('solicitante')
    incidentes = Prestamo.objects.values('solicitante').annotate(solicitante_count=Count('solicitante')).filter(solicitante_count__gt=0, solicitante_count__lt=3, incidencia=True).values('solicitante')
    a_bloquear = Prestamo.objects.values('solicitante').annotate(solicitante_count=Count('solicitante')).filter(solicitante_count__gt=2, incidencia=True).values('solicitante')
    incidentes_count = Prestamo.objects.values('solicitante').annotate(solicitante_count=Count('solicitante')).filter(solicitante_count__gt=0, solicitante_count__lt=3, incidencia=True).values('solicitante_count')
    a_bloquear_count = Prestamo.objects.values('solicitante').annotate(solicitante_count=Count('solicitante')).filter(solicitante_count__gt=2, incidencia=True).values('solicitante_count')
    sol_regulares = Solicitante.objects.filter(matricula__in=regulares).exclude(matricula__in=incidentes).exclude(matricula__in=a_bloquear) #Truco sucio. Corregir.
    sol_incidentes = Solicitante.objects.filter(matricula__in=incidentes)
    sol_a_bloquear = Solicitante.objects.filter(matricula__in=a_bloquear)

    return render(request, 'solicitantes.html', {'todos': todos, 'regulares': sol_regulares, 'incidentes':sol_incidentes, 'bloqueados':sol_a_bloquear, 'incidentes_count':incidentes_count, 'bloqueados_count':a_bloquear_count, 'incidentes_all':incidentes_all})

def bloquear_solicitante(request, id):

    if request.method == 'POST':
        try:
            solicitante_sel = Solicitante.objects.get(matricula = id)
            
            if solicitante_sel.bloqueado == False:
                solicitante_sel.bloqueado = True
            else:
                solicitante_sel.bloqueado = False

            solicitante_sel.save()
            return redirect('solicitantes')

        except Solicitante.DoesNotExist:
            return HttpResponse('Solicitante no encontrado', status=404)
        except Exception:
            return HttpResponse('Internal Error', status=500)
    return HttpResponse('Solo POST requests', status=405)

def exportar_csv_prestamos(request):

    output = []
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment;filename="prestamos_data.csv"'

    writer = csv.writer(response)
    query_set = Prestamo.objects.all()
    writer.writerow(['id', 'Solicitante', 'Proyector', 'Materia', 'Aula', 'Fecha_hora', 'Hora_fin', 'Bocinas', 'Incidencia', 'Cerrado'])
    for prestamo in query_set:
        output.append([prestamo.id, prestamo.solicitante, prestamo.proyector, prestamo.materia, prestamo.aula, prestamo.fecha_hora, prestamo.hora_fin, prestamo.bocinas, prestamo.incidencia, prestamo.cerrado])
    writer.writerows(output)
    return response

def exportar_csv_solicitantes(request):

    output = []
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment;filename="solicitantes_data.csv"'

    writer = csv.writer(response)
    query_set = Solicitante.objects.all()
    writer.writerow(['No. Control', 'Nombre', 'Apellido_Paterno', 'Apellido_Materno', 'Carrera', 'Bloqueado'])
    for solicitante in query_set:
        output.append([solicitante.matricula, solicitante.nombre, solicitante.ap_pat, solicitante.ap_mat, solicitante.carrera, solicitante.bloqueado])
    writer.writerows(output)
    return response


    







    
        

