# Generated by Django 2.0.6 on 2019-03-02 07:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('prestaproyector', '0008_auto_20190302_0001'),
    ]

    operations = [
        migrations.AlterField(
            model_name='prestamo',
            name='materia',
            field=models.CharField(max_length=30, null=True),
        ),
    ]
