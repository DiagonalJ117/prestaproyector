from django.db import models
from django import forms
from django.utils.safestring import mark_safe
from .models import Prestamo, Proyector, Solicitante, Materia, Componente
from django.core.exceptions import ValidationError
from django.core.validators import MinValueValidator

class SolicitudProyector_Form(forms.ModelForm):
    required_css_class = 'required'
    componentes = forms.ModelMultipleChoiceField(queryset=Componente.objects.filter(disponible=True), widget=forms.CheckboxSelectMultiple, required=False)
    


    def clean_solicitante(self):
        solicitante = self.cleaned_data.get('solicitante')
        try:
            Solicitante.objects.get(matricula=solicitante)
        except Solicitante.DoesNotExist:
            msg = forms.ValidationError(mark_safe('Matricula no existente en el sistema. <strong>Verifique que lo haya ingresado correctamente</strong> o <a href="/prestaproyector/registrar_solicitante/">Registrese aquí</a>'))
            self.add_error('solicitante', msg)
            raise msg
        return solicitante

    def clean_aula(self):
        aula = self.cleaned_data['aula'].upper()
        return aula

    class Meta:
        model = Prestamo
        fields = ['proyector', 'solicitante', 'materia', 'componentes', 'aula', 'hora_fin', 'incidencia', 'cerrado']
        labels = {
            'aula': 'Aula en donde se utilizará'
        }

class RegistroSolicitante_Form(forms.ModelForm):
    required_css_class = 'required'

    def clean_matricula(self):
        matricula = self.cleaned_data.get('matricula').upper()
        r = Solicitante.objects.filter(matricula=matricula)
        if r.count():
            raise ValidationError("Matricula ya existente")
        return matricula
        
    def clean_carrera(self):
        carrera = self.cleaned_data.get('carrera').upper()
        return carrera
        
    class Meta:
        model = Solicitante
        fields = ['matricula', 'nombre', 'ap_pat', 'ap_mat', 'carrera']
        labels = {
            'matricula': 'No. Control',
            'ap_pat': 'Apellido Paterno',
            'ap_mat': 'Apellido Materno'
        }

class RegistroMateria_Form(forms.ModelForm):
    required_css_class = 'required'
    def clean(self):
        id_materia = self.cleaned_data['id_materia'].upper()
        
    
    class Meta:
        model = Materia
        fields = ['id_materia', 'nombre']


        
class RegistroProyector_Form(forms.ModelForm):
    required_css_class = 'required'
    def clean(self):
        id_proyector = self.cleaned_data.get('id_proyector')
        r = Proyector.objects.filter(id_proyector=id_proyector)
        if r.count():
            raise ValidationError("Proyector ya existente")
        
    class Meta:    
        model = Proyector
        fields = ['id_proyector', 'descripcion', 'disponible']

class RegistroComponente_Form(forms.ModelForm):
    required_css_class = 'required'
    def clean(self):
        id_componente = self.cleaned_data.get('id_componente')
        r = Componente.objects.filter(id_componente=id_componente)
        if r.count():
            raise ValidationError("Componente ya existente")
        
    class Meta:
        model = Componente
        fields = ['id_componente', 'nombre_componente', 'tipo_componente', 'descripcion', 'proyector']

class CheckSolicitante_Form(forms.ModelForm):
    required_css_class = 'required'
    def clean_solicitante(self):
        solicitante = self.cleaned_data.get('solicitante')
        try:
            Solicitante.objects.get(matricula=solicitante)
        except Solicitante.DoesNotExist:
            msg = forms.ValidationError(mark_safe('Matricula no existente en el sistema. <strong>Verifique que lo haya ingresado correctamente</strong> o <a href="/prestaproyector/registrar_solicitante/">Registrese aquí</a>'))
            self.add_error('solicitante', msg)
            raise msg
        return solicitante

    class Meta:
        model = Solicitante
        fields = ['matricula']


