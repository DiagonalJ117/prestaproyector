from django import template
from django.shortcuts import get_object_or_404

register = template.Library()

@register.filter
def materias_solicitante(materias, matricula):
    return materias.filter(matricula=matricula)

@register.filter
def de_solicitante(prestamo, solicitante):
    return prestamo.filter(solicitante=solicitante).order_by('solicitante_count').first()

@register.filter
def get_value_in_qs(queryset, key):
    return queryset.values_list(key, flat=True).get(solicitante=key)

@register.filter
def get_count(cosa):
    return cosa.count()