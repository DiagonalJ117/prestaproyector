"""prestaproyector URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from prestaproyector import views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls.static import static
from django.conf import settings



urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.solicitud, name="solicitud"),
    path('sol_proyector/', views.prefill_solicitud, name="prefill_solicitud"),
    path('enviar_solicitud/', views.solicitud_proyector, name="sol_proyector"),
    path('registrar_proyector/', views.registrar_proyector, name="add_proyector"),
    path('registrar_componente/', views.registrar_componente, name="add_componente"),
    path('registrar_solicitante/', views.registrar_solicitante, name="add_solicitante"),
    path('exportar_csvs_prestamos/', views.exportar_csv_prestamos, name="exportar_csv_prestamos"),
    path('exportar_csvs_solicitantes/', views.exportar_csv_solicitantes, name="exportar_csv_solicitantes"),
    #path('exportar_csvs_proyectores/', views.exportar_csv_proyectores, name="exportar_csvs_proyectores"),
    path('accounts/', include('accounts.urls')),
    path('dashboard/', views.dashboard, name="dashboard"),
    path('lista_prestamos/', views.lista_prestamos, name="lista_prestamos"),
    path('devolucion/<id>', views.devolucion, name="devolucion"),
    path('marcar_incidencia/<id>', views.marcar_incidencia, name="marcar_incidencia"),
    path('proyectores/', views.status_proyectores, name="proyectores"),
    path('proyector_status_update/<id>', views.proyector_status_update, name="proyector_status_update"),
    path('componente_status_update/<id>', views.componente_status_update, name="componente_status_update"),
    path('solicitantes/', views.solicitantes, name="solicitantes"),
    path('bloquear_solicitante/<id>', views.bloquear_solicitante, name="bloquear_solicitante")
]
