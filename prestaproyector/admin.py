from django.contrib import admin
from .models import Materia, Solicitante, Proyector, Prestamo

# Register your models here.
admin.site.register(Materia)
admin.site.register(Solicitante)
admin.site.register(Proyector)
admin.site.register(Prestamo)

admin.site.site_header = "Solicitud de Proyectores"
admin.site.site_title = "Panel de Admin"