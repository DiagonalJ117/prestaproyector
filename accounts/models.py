from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext as _

# Create your models here.

class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    username = models.CharField(max_length=20)
    email = models.EmailField()

    class Meta:
        permissions = (
            ('is_superuser', _('Is superuser')),
            ('is_staff', _('Is staff')),
        )
