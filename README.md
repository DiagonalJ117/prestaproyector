
# Registro de Préstamo de Proyectores

Plataforma para el registro de prestamos y devoluciones de proyectores y sus respectivos componentes.

* Registro de fecha, hora, aula y asunto para préstamo del proyector y hora de devolución

* Registro de incidencias por retardo en devolución o extravío de proyector o componente.

* Bloqueo a usuarios con mayor a `n` incidencias

## Backend

Backend hecho en Django 2.2

<img src="https://static.djangoproject.com/img/logos/django-logo-positive.png" align="center">

## Frontend

Frontend hecho con Bootstrap 4

<img src="https://cdn.worldvectorlogo.com/logos/bootstrap-4.svg" align="center">
