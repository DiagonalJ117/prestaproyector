"""
WSGI config for prestaproyector project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.0/howto/deployment/wsgi/
"""

import os, sys

sys.path.append('C:/Users/asus/Bitnami Django Stack projects/prestaproyector')
os.environ["DJANGO_SETTINGS_MODULE"] = "prestaproyector.settings"

from django.core.wsgi import get_wsgi_application
os.environ["PYTHON_EGG_CACHE"] = "C:/Users/asus/Bitnami Django Stack projects/prestaproyector/egg_cache"


application = get_wsgi_application()
